# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: "Chicago" }, { name: "Copenhagen" }])
#   Mayor.create(name: "Emanuel", city: cities.first)

code = '<!DOCTYPE html>
<html>
  <head>
    <title>Trololo</title>
    <link rel="stylesheet" type="text/css" href="/html/css/phone.css" />
    <style>
      #p8{
      width: 80px;
      height: 80px;
      top: 100px;
      position: absolute;
      background: url("../assets/buttons/over/8.png");
      background-size: cover;
      }
      #p8:hover {
      background: url("../assets/buttons/hover/8.png");
      background-size: cover;
      }
      #p2{
      width: 80px;
      height: 80px;
      position: absolute;
      left: 150px;
      top: 100px;
      background: url("../assets/buttons/over/2.png");
      background-size: cover;
      }
      #p2:hover {
      background: url("../assets/buttons/hover/2.png");
      background-size: cover;
      }
      #p7{
      width: 150px;
      height: 150px;
      position: absolute;
      left: 300px;
      top: 60px;
      background: url("../assets/buttons/over/7.png");
      background-size: cover;
      }
      #p7:hover {
      background: url("../assets/buttons/hover/7.png");
      background-size: cover;
      }
    </style>
  </head>
<body>
  <div class="container">
    <div id="p8" onMSpointerdown ="window.external.Notify("send {LEFT down}");" onMSpointerup="window.external.Notify("send {LEFT up}");"></div>
    <div id="p2" onMSpointerdown ="window.external.Notify("send {RIGHT down}");" onMSpointerup="window.external.Notify("send {RIGHT up}");"></div>
    <div id="p7" onMSpointerdown ="window.external.Notify("send {UP down}");" onMSpointerup="window.external.Notify("send {UP up}");"></div>
  </div>
</body>
</html>'


code2 = '<!DOCTYPE html>
<html>
<head>
  <title>Trololo</title>
  <style>
    body {
      width: 200px;
      height: 200px;      
      margin: 0;
    }
    
    .container {
      width: 800px;
      height: 480px;
      background: url("assets/bg.png");
      background-size: cover;
      position: relative;
      left: 0px;
      top: 0px;
    }
    #p8{
      width: 130px;
      height: 130px;
      position: absolute;
      left: 50px;
      top: 280px;
      background: url("assets/style1/over/8.png");
      background-size: cover;
    }
    #p8:hover {
      background: url("assets/style1/hover/8.png");
      background-size: cover;
    } 
    #p2{
      width: 130px;
      height: 130px;
      position: absolute;
      left: 250px;
      top: 280px;
      background: url("assets/style1/over/2.png");
      background-size: cover;
    }
    #p2:hover {
      background: url("assets/style1/hover/2.png");
      background-size: cover;
    }
    #p7{
      width: 150px;
      height: 150px;
      position: absolute;
      left: 550px;
      top: 250px;
      background: url("assets/style1/over/7.png");
      background-size: cover;
    }
    #p7:hover {
      background: url("assets/style1/hover/7.png");
      background-size: cover;
    }
  </style>
</head>
<body>
  <div class="container">
    <div id="p8"></div>
    <div id="p2"></div>
    <div id="p7"></div>
  </div>
</body>
</html>
  '
Style.create(code: code, link_to_image: 'http://gamew.in/assets/profile/profile-1.png')
Style.create(code: code2, link_to_image: 'http://gamew.in/assets/profile/profile-2.png')